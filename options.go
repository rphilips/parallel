package parallel

type Config struct {
	max int
}

type Option func(*Config)

func NewCfg(opts []Option) Config {
	c := new(Config)
	for _, opt := range opts {
		opt(c)
	}
	return *c
}

func WithMax(max int) Option {
	return func(c *Config) {
		c.max = max
	}
}
