package parallel

import (
	"runtime"
)

// NMap works on every integer in the range from 0 to ... n-1 in parallel
// The function works with 2 arguments and an (optional) option
//
//   - an integer n
//   - a worker function with one argument (an integer) which produces a result (type any) and an error
//
// The (optional) option gives a maximum of go-routines doing work.
//
// The function return 2 values:
//
//   - resultlist: a slice of any (the result of each individual worker)
//   - errorlist: a slice of error (the error of each individual worker)
//
// Options:
//
//	WithMax(int): maximum workers (the default is lower than runtime.GOMAXPROCS(-1))
//
// Example:
//
//	resultlist, errorlist := parallel.NMap(100, func(m int) {....} (any, err), parallel.WithMax(7))
func NMap(n int, fn func(m int) (r any, err error), options ...Option) (resultlist []any, errorlist []error) {
	if n == 0 {
		return
	}

	config := NewCfg(options)

	resultlist = make([]any, n)
	errorlist = make([]error, n)
	if n == 1 {
		resultlist[0], errorlist[0] = fn(0)
		return
	}

	pmax := config.max
	if pmax == 0 {
		pmax = runtime.GOMAXPROCS(-1)
		if pmax > 1 {
			pmax = pmax - 1
		}
	}
	if n < pmax {
		pmax = n
	}

	if pmax < 1 {
		pmax = 1
	}

	// Channel for blocking until work done

	done := make(chan bool, pmax)

	// Setup producer

	jobs := make(chan int)
	go func(n int, jobs chan<- int) {
		for i := 0; i < n; i++ {
			jobs <- i
		}
		close(jobs)
	}(n, jobs)

	// Setup max consumers
	for i := 0; i < pmax; i++ {
		go func(jobs <-chan int, done chan<- bool) {
			for job := range jobs {
				resultlist[job], errorlist[job] = fn(job)
			}
			done <- true
		}(jobs, done)
	}
	for i := 0; i < pmax; i++ {
		<-done
	}
	close(done)
	return
}
