package parallel

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"
)

func TestParallel(t *testing.T) {
	dir, err := os.MkdirTemp("", "parallel")
	if err != nil {
		t.Errorf("Error: %s", err)
		return
	}

	for i := 0; i < 1000; i++ {
		fname := filepath.Join(dir, fmt.Sprintf("f%d", i))
		body := filepath.Join(dir, fmt.Sprintf("a%d", i))
		err := os.WriteFile(fname, []byte(body), 0777)
		if err != nil {
			t.Errorf("Error in %s: %s", dir, err)
			return
		}
	}
	fn := func(m int) (r any, err error) {
		fname := filepath.Join(dir, fmt.Sprintf("f%d", m))
		body := filepath.Join(dir, fmt.Sprintf("a%d", m))
		found, err := os.ReadFile(fname)
		if err != nil {
			return false, err
		}
		return body == string(found), nil
	}

	resultlist, errorlist := NMap(1000, fn)

	for i := 0; i < 1000; i++ {
		if errorlist != nil {
			if err != nil {
				t.Errorf("Error in %s %d: %s", dir, i, err)
			}
		}
	}

	for i := 0; i < 1000; i++ {
		if !resultlist[i].(bool) {
			t.Errorf("Error in %s %d", dir, i)
		}
	}

	os.RemoveAll(dir)
}
